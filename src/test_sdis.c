/* Copyright (C) 2016-2024 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "sdis.h"
#include "test_sdis_utils.h"

int
main(int argc, char** argv)
{
  struct sdis_info info = SDIS_INFO_NULL;
  (void)argc, (void)argv;

  BA(sdis_get_info(NULL));
  OK(sdis_get_info(&info));
#ifdef SDIS_ENABLE_MPI
  CHK(info.mpi_enabled);
#else
  CHK(!info.mpi_enabled);
#endif
  return 0;
}

