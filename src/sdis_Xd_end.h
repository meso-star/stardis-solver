/* Copyright (C) 2016-2024 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef SDIS_XD_BEGIN_H__
  #error "The sdis_Xd_begin.h file must be included priorly to this file."
#endif

#undef SDIS_XD_DIMENSION
#undef DIM

#undef sXd
#undef SXD_HIT_NONE
#undef SXD_HIT_NULL
#undef SXD_HIT_NULL__
#undef SXD_POSITION
#undef SXD_GEOMETRY_NORMAL
#undef SXD_VERTEX_DATA_NULL
#undef SXD
#undef SXD_FLOAT2
#undef SXD_FLOAT3
#undef SXD_FLOATX
#undef SXD_GET_PRIMITIVE
#undef SXD_SAMPLE
#undef SXD_TRACE
#undef SXD_PRIMITIVE_NULL
#undef SXD_PRIMITIVE_EQ

#undef dX
#undef fX
#undef fX_set_dX
#undef fXX_mulfX
#undef dX_set_fX

#undef FORMAT_VECX
#undef SPLITX

#undef SDIS_XD_BEGIN_H__
